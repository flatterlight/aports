# Contributor: Galen Abell <galen@galenabell.com>
# Maintainer: Galen Abell <galen@galenabell.com>
pkgname=py3-tldextract
_pyname=tldextract
pkgver=3.1.1
pkgrel=0
pkgdesc="Accurately separate the TLD from the registered domain and subdomains of a URL"
url="https://github.com/john-kurkowski/tldextract"
arch="noarch"
license="BSD-3-Clause"
depends="
	python3
	py3-idna
	py3-requests
	py3-requests-file
	py3-filelock
	"
makedepends="py3-setuptools"
checkdepends="py3-pytest py3-responses"
source="$_pyname-$pkgver.tar.gz::https://github.com/john-kurkowski/tldextract/archive/$pkgver.tar.gz"
builddir="$srcdir/$_pyname-$pkgver"

prepare() {
	default_prepare

	# Generate _version.py ourselves since we don't use setuptools_scm
	cat <<- EOF > tldextract/_version.py
	# coding: utf-8
	version = '$pkgver'
	version_tuple = ($(echo $pkgver | sed 's/\./, /g'))
	EOF

	sed -e '/setuptools_scm/d' \
		-e "s/use_scm_version.*/version='$pkgver',/" \
		-i setup.py
}

build() {
	python3 setup.py build
}

check() {
	python3 setup.py test
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}
sha512sums="
a63229ab36686c95acd52bdaa6a91a7cb62aa8330b023c7e66fa05148a6bc82803e612e306f44edffa38c9997f8d0ae2e2cb6bd394a798e559857e3302e9a6ef  tldextract-3.1.1.tar.gz
"
